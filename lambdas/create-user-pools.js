const AWS = require("aws-sdk");
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

exports.handler = async (event, context) => {

    // map over each record that was created
    // then generate a user pool from all applications

    const applicationRecords = event.Records
        .filter(record => {
            // first check if it is a dynamodb invocation and there is a NewImage instance
            if (!(record.dynamodb && record.dynamodb.NewImage)) return false;
            // then we check the type of the entity being created 
            return (newImage.entityType && newImage.entityType.S === "application");
        });



    console.log(applicationRecords);

    // then we convert the applicationRecords into Promises that create a user pool for each application
    const responses = applicationRecords.map(appRecords => {

        const applicationId = newImage.applicationId.S;

        console.log(`Creating user pool for application ${applicationId}`);

        const params = {
            PoolName: `${applicationId}-user-pool`,
            AccountRecoverySetting: {
                RecoveryMechanisms: [
                    {
                        Name: "verified_email",
                        Priority: 1
                    },
                ]
            },
            AliasAttributes: [
                "email",
            ],
            AutoVerifiedAttributes: [
                "email",
            ],
            UsernameConfiguration: {
                CaseSensitive: true /* required */
            },
        };

        return await cognitoidentityserviceprovider.createUserPool(params).promise().catch(e => `error occured with app ${applicationId}: ${err}. stack: ${err.stack}`);

    });

    const response = await Promise.all(responses);

    response.forEach(res => console.log(res));

    return `Successfully processed ${event.Records.length} records.`;
};