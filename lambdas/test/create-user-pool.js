const AWS = require("aws-sdk");
AWS.config.update({ region: "eu-west-1" });
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
require("dotenv").config();

const applicationId = "lalala";

const params = {
    PoolName: `${applicationId}-user-pool`,
    AccountRecoverySetting: {
        RecoveryMechanisms: [
            {
                Name: "verified_email",
                Priority: 1
            },
        ]
    },
    AliasAttributes: [
        "email",
    ],
    AutoVerifiedAttributes: [
        "email",
    ],
    // Policies: {
    //     PasswordPolicy: {
    //         MinimumLength: 'NUMBER_VALUE',
    //         RequireLowercase: true || false,
    //         RequireNumbers: true || false,
    //         RequireSymbols: true || false,
    //         RequireUppercase: true || false,
    //         TemporaryPasswordValidityDays: 'NUMBER_VALUE'
    //     }
    // },
    // Schema: [
    //     {
    //         AttributeDataType: String | Number | DateTime | Boolean,
    //         DeveloperOnlyAttribute: true || false,
    //         Mutable: true || false,
    //         Name: 'STRING_VALUE',
    //         NumberAttributeConstraints: {
    //             MaxValue: 'STRING_VALUE',
    //             MinValue: 'STRING_VALUE'
    //         },
    //         Required: true || false,
    //         StringAttributeConstraints: {
    //             MaxLength: 'STRING_VALUE',
    //             MinLength: 'STRING_VALUE'
    //         }
    //     },
    //     /* more items */
    // ],
    // SmsAuthenticationMessage: 'STRING_VALUE',
    // SmsConfiguration: {
    //     SnsCallerArn: 'STRING_VALUE', /* required */
    //     ExternalId: 'STRING_VALUE'
    // },
    // SmsVerificationMessage: 'STRING_VALUE',
    // UserPoolAddOns: {
    //     AdvancedSecurityMode: OFF | AUDIT | ENFORCED /* required */
    // },
    // UserPoolTags: {
    //     '<TagKeysType>': 'STRING_VALUE',
    //     /* '<TagKeysType>': ... */
    // },
    // UsernameAttributes: [
    //     phone_number | email,
    //     /* more items */
    // ],
    UsernameConfiguration: {
        CaseSensitive: true /* required */
    },
    // VerificationMessageTemplate: {
    //     DefaultEmailOption: CONFIRM_WITH_LINK | CONFIRM_WITH_CODE,
    //     EmailMessage: 'STRING_VALUE',
    //     EmailMessageByLink: 'STRING_VALUE',
    //     EmailSubject: 'STRING_VALUE',
    //     EmailSubjectByLink: 'STRING_VALUE',
    //     SmsMessage: 'STRING_VALUE'
    // }
};


cognitoidentityserviceprovider.createUserPool(params).promise().then(response => console.log(`Successful: ${response}`)).catch(err => console.error(err));