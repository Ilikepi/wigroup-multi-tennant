const AWS = require("aws-sdk");
AWS.config.update({ region: "eu-west-1" });
const docClient = new AWS.DynamoDB.DocumentClient();
const { transform } = require("nosqlist");
const { v4 } = require("uuid");
require("dotenv").config();


const applicationTransformer = {
    "PK": "ADMIN#{adminId}",
    "SK": "APPLICATION#{applicationId}",
    "GSIPK": "APPLICATION#{applicationId}",
    "GSISK": "METADATA#{applicationId}",
    "createdAt": () => Date.now(),
    "entityType": () => "application"
}


const addItem = async function (item) {
    const params = {
        TableName: "multi-tennant-table",
        Item: item
    };

    try {
        return await docClient.put(params).promise();
    } catch (err) {
        return err;
    }


};

const item = transform(applicationTransformer, {
    adminId: "a16f6f4e-458a-4e47-b435-4ecd7a6202a3",
    applicationId: v4()
});

addItem(item).then(response => console.log(`Successful: ${response}`)).catch(err => console.error(err, err.stack));