const AWS = require("aws-sdk");
AWS.config.update({ region: "eu-west-1" });
const docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event, context) => {


    const { sub, email } = event.request.userAttributes;

    const item = {
        adminId: sub,
        email,
        PK: `ADMIN#${sub}`,
        SK: `METADATA#${sub}`
    };

    const params = {
        TableName: "multi-tennant-table",
        Item: item
    };

    try {
        await docClient.put(params).promise();
    } catch (err) {
        console.error(err);
    }



    return event;
};

