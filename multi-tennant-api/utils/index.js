
const config = require("../config");
const jwt = require('jsonwebtoken');
const jwkToPem = require('jwk-to-pem');

const fetch = require("node-fetch");

module.exports.decodeToken = async (token) => {

    const response = await fetch(`https://cognito-idp.${config.AWS_DEFAULT_REGION}.amazonaws.com/${config.AWS_USER_POOL}/.well-known/jwks.json`).then(res => res.json())

    const jwk = response.keys[0];

    const pem = jwkToPem(jwk);

    return new Promise((resolve, reject) => {
        jwt.verify(token, pem, { algorithms: ['RS256'] }, (err, decodedToken) => {
            if (err) reject(err);

            resolve(decodedToken);
        });
    });
}
