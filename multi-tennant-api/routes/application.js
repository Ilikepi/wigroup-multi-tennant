const { Router } = require("express")
const router = Router();
const { createApplication, getApplication } = require("../controllers/application");
const { HttpError } = require("../error");

router.get("/:id", (req, res) => {
    const { id } = req.params;
    getApplication(id).then(result => {
        if (result instanceof HttpError) {
            res.status(result.code).json({ message: result.message });
            return;
        };

        res.status(200).json(result);
    });
});

router.post("/", (req, res) => {
    createApplication(req.body).then(result => {
        if (result instanceof HttpError) {
            res.status(result.code).json({ message: result.message });
            return;
        };

        res.status(201).json(result);
    });
});


module.exports = router;