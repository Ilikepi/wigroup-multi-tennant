const { Router } = require("express")
const router = Router();
const { createAdmin, getAdmin, getApplicationsForAdmin, getApplicationsForAdminFromToken, createApplicationUsingToken } = require("../controllers/administrator");
const { HttpError } = require("../error");


router.get("/:id", (req, res) => {
    const { id } = req.params;

    if (!id) res.status(400).json({ message: "Indicate a administrator" });

    getAdmin(id).then((result) => {

        if (result instanceof HttpError) {
            res.status(result.code).json({ message: result.message });
            return;
        }

        res.status(200).json(result);
    });
});

router.get("/me/applications", (req, res) => {

    getApplicationsForAdminFromToken(req.token).then((result) => {

        if (result instanceof HttpError) {
            res.status(result.code).json({ message: result.message });
            return;
        }

        res.status(200).json(result);
    });
});

router.post("/me/application", (req, res) => {

    createApplicationUsingToken(req.token, req.body).then((result) => {

        if (result instanceof HttpError) {
            res.status(result.code).json({ message: result.message });
            return;
        }

        res.status(201).json(result);
    });
})

router.get("/:id/applications", (req, res) => {
    const { id } = req.params;

    getApplicationsForAdmin(id).then((result) => {

        if (result instanceof HttpError) {
            res.status(result.code).json({ message: result.message });
            return;
        }

        res.status(200).json(result);
    });
});




router.post("/", (req, res) => {

    createAdmin(req.body).then(result => {
        if (result instanceof HttpError) {
            res.status(result.code).json({ message: result.message });
            return;
        };

        res.status(201).json(result);
    });
});


module.exports = router;