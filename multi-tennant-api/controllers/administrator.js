const { getAdministratorById, addItem, getAllApplicationsForAdministrator } = require("../repositories");
const { HttpError } = require("../error");
const { administrator, application } = require("../convert");
const { transform } = require("nosqlist");
const schema = require("../json_schema");
const Ajv = require("ajv");
const transformItem = require("nosqlist/src/transformItem");
const uuidv4 = require("uuid").v4;
const { decodeToken } = require("../utils");


module.exports.createAdmin = async function (adminData) {

    const validate = (new Ajv()).compile(schema.administrator);
    const valid = validate(adminData);

    if (!valid) return new HttpError(400, validate.errors[0]);

    const newAdministrator = { ...adminData };
    const transformedAdmin = transform(administrator, newAdministrator);

    return addItem(transformedAdmin).catch(err => new HttpError(400, String(err)));
}

module.exports.getAdmin = async function (adminId) {

    return await getAdministratorById(adminId).catch(err => new HttpError(400, String(err)));
}

module.exports.createApplicationUsingToken = async function (token, data) {

    const adminId = (await decodeToken(token)).sub;

    console.log(adminId);
    const app = { adminId, ...data };

    const validate = (new Ajv()).compile(schema.application); // options can be passed, e.g. {allErrors: true}
    const valid = validate(app);

    if (!valid) return new HttpError(400, validate.errors[0]);


    const newApplication = { ...app, applicationId: uuidv4() };
    const transformedApplication = transform(application, newApplication);


    return addItem(transformedApplication).catch(err => new HttpError(400, String(err)));
}


module.exports.getApplicationsForAdmin = async function (adminId) {
    return await getAllApplicationsForAdministrator(adminId).catch(err => new HttpError(400, err));
}

module.exports.getApplicationsForAdminFromToken = async function (jwtToken) {
    const decodedToken = await decodeToken(jwtToken);
    return await getAllApplicationsForAdministrator(decodedToken.sub).catch(err => new HttpError(400, err));
}