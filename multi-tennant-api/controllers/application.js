const { addItem, getApplicationFromId } = require("../repositories");
const { HttpError } = require("../error");
const { application } = require("../convert");
const { transform } = require("nosqlist");
const schema = require("../json_schema");
const Ajv = require("ajv");
const uuidv4 = require("uuid").v4;

module.exports.createApplication = async function (applicationData) {
    const validate = (new Ajv()).compile(schema.application); // options can be passed, e.g. {allErrors: true}
    const valid = validate(applicationData);

    console.log(valid, validate.errors);
    if (!valid) return new HttpError(400, validate.errors[0]);


    const newApplication = { ...applicationData, applicationId: uuidv4() };
    const transformedApplication = transform(application, newApplication);


    return addItem(transformedApplication).catch(err => new HttpError(400, String(err)));
}

module.exports.getApplicationsForAdmin = async function (adminId) {



}

module.exports.getApplication = async function (applicationId) {

    return await getApplicationFromId(applicationId).catch(err => new HttpError(400, err));

}
