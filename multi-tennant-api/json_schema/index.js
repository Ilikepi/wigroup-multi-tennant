module.exports.administrator = {
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": { "adminId": { "type": "string" }, "username": { "type": "string" }, },
    "required": ["username", "adminId"]
};

module.exports.application = {
    "$schema": "http://json-schema.org/schema#",
    "type": "object",
    "properties": { "adminId": { "type": "string" }, "description": { "type": "string" } },
    "required": ["adminId", "description"]
};