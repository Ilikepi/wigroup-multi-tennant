const AWS = require("aws-sdk");
const { TENNANT_TABLE } = require("../config");
AWS.config.update({ region: "eu-west-1" });
const docClient = new AWS.DynamoDB.DocumentClient();
const { transformItem } = require("nosqlist");

const { administrator, application } = require("../convert");



module.exports.addItem = async function (item) {

    console.log(TENNANT_TABLE);
    const params = {
        TableName: TENNANT_TABLE,
        Item: item
    };

    try {
        return await docClient.put(params).promise();
    } catch (err) {
        return err;
    }


};


module.exports.getAdministratorById = async function (adminId) {

    const pk = transformItem(administrator.PK, { adminId });
    const sk = transformItem(administrator.SK, { adminId });

    const params = {
        TableName: TENNANT_TABLE,
        Key: { "PK": pk, "SK": sk }
    };

    try {
        return (await docClient.get(params).promise()).Item;
    } catch (err) {
        return err;
    }

}

module.exports.getAllApplicationsForAdministrator = async function (adminId) {

    const pk = transformItem(application.PK, { adminId });

    console.log(pk);

    const params = {
        TableName: TENNANT_TABLE,
        KeyConditionExpression: "#PK = :pk and begins_with(#SK, :sk)",
        ExpressionAttributeNames: {
            "#PK": "PK",
            "#SK": "SK"
        },
        ExpressionAttributeValues: {
            ":pk": pk,
            ":sk": "APPLICATION#"
        },
    };


    try {
        return (await docClient.query(params).promise()).Items;
    } catch (err) {
        return err;
    }

}

module.exports.getApplicationFromId = async function (applicationId) {
    const pk = transformItem(application.GSIPK, { applicationId });
    const sk = transformItem(application.GSISK, { applicationId });

    const params = {
        TableName: TENNANT_TABLE,
        IndexName: "GSIPK-GSISK-index",
        KeyConditionExpression: "GSIPK = :pk and GSISK = :sk",
        ExpressionAttributeValues: {
            ":pk": pk,
            ":sk": sk
        },
    };

    try {
        return (await docClient.query(params).promise()).Items[0];
    } catch (err) {
        return err;
    }
}