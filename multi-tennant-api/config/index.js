require("dotenv").config()
module.exports = {
    PORT: process.env.PORT,
    TENNANT_TABLE: process.env.TENNANT_TABLE,
    AWS_DEFAULT_REGION: process.env.AWS_DEFAULT_REGION,
    AWS_USER_POOL: process.env.AWS_USER_POOL
}