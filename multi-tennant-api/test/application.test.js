//a collection of test cases that test a specific component
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe("Checking Application", () => {

    it("should return 200 on get", (done) => {

        chai.request(server)
            .get('/application')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });

    })

    it("should return 200 on get with id", (done) => {

        chai.request(server)
            .get('/application/1')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.id.should.eql("1")
                done();
            });

    })

    it("should return 200 on post", (done) => {

        chai.request(server)
            .post('/application')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });

    })


})