module.exports.administrator = {
    "PK": "ADMIN#{adminId}",
    "SK": "METADATA#{adminId}",
    "createdAt": () => Date.now()
};

module.exports.application = {
    "PK": "ADMIN#{adminId}",
    "SK": "APPLICATION#{applicationId}",
    "GSIPK": "APPLICATION#{applicationId}",
    "GSISK": "METADATA#{applicationId}",
    "createdAt": () => Date.now(),
    "entityType": () => "application"
}