const express = require("express");
const app = express();
const cors = require("cors");
const helmet = require("helmet");
const config = require("./config");
const bodyParser = require("body-parser");
const bearerToken = require('express-bearer-token');


// middleware
app.use(cors());
app.use(helmet());
app.use(bodyParser.json({ strict: true }));
app.use(bearerToken());


// routes
const adminRoutes = require("./routes/administrator");
const applicationRoutes = require("./routes/application");

app.use("/administrator", adminRoutes);
app.use("/application", applicationRoutes);

app.get("/ping", (req, res) => {
    res.status(200).send("healthy");
});

app.listen(config.PORT, () => console.log(`Listening on port ${config.PORT}`));

module.exports = app;